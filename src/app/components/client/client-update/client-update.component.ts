import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../client.service';
import { Client } from '../client.interface';

@Component({
  selector: 'app-client-update',
  templateUrl: './client-update.component.html',
  styleUrls: ['./client-update.component.styl']
})
export class ClientUpdateComponent implements OnInit {
  client: Client;
  clientFromUpdateGroup: FormGroup;
  constructor(private activatedRoute: ActivatedRoute, private clientService: ClientService, private formBuilder: FormBuilder, private router: Router) { 
    this.clientFromUpdateGroup = this.formBuilder.group({
      id:[],
      documentNumber:['',[
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(10)
      ]],
      name:['',[Validators.required]],
      email:[''],
      phoneNumber:['']
    });
  }
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID GET', id);
    this.clientService.getClientById(id)
    .subscribe(res => {
      console.warn('DATA UPDATE', res);
      this.client = res;
      this.loadForm(this.client);
    });
  }

  private loadForm(client: Client){
    this.clientFromUpdateGroup.patchValue({
      id: client.id,
      name: client.name,
      email: client.email,
      documentNumber: client.documentNumber,
      phoneNumber: client.phoneNumber
    })
  }

}
