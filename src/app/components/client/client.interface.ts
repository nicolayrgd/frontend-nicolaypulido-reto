export interface Client {
    id?: number;
    name: string;
    email: string;
    phoneNumber: number;
    documentNumber: number;
}
