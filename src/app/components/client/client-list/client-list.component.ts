import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { Client } from '../../client';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.styl']
})
export class ClientListComponent implements OnInit {
  clientList: Client[];
  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.clientService.query()
    .subscribe(res =>{
      this.clientList = res;
    });
  }

}
