import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from '../client';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor(private http: HttpClient) { }

  public query(): Observable<Client[]>{
    return this.http.get<Client[]>(`${environment.END_POINT}/api/client`)
    .pipe(map(res =>{
      return res;
    }));
  }
  public saveClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.END_POINT}/client`, client)
      .pipe(map(res =>{
      return res;
    }));
  }
  public getClientById(id: string): Observable<Client>{
    return this.http.get<Client>(`${environment.END_POINT}/api/client/${id}`)
    .pipe(map(res =>{
      return res;
    }));   
  }
  public updateClient(client: Client): Observable<Client>{
    return this.http.put<Client>(`${environment.END_POINT}/client`, client)
    .pipe(map(res =>{
      return res;
    }));
  }
}
