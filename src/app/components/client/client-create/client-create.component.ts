import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.styl']
})
export class ClientCreateComponent implements OnInit {
  clientFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private clientService: ClientService) {
    this.clientFormGroup = this.formBuilder.group({
      documentNumber:['',[
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(10)
      ]],
      name:['',[Validators.required]],
      email:[''],
      phoneNumber:['']
    })
   }

  ngOnInit() {
  }

  public saveClient(){
    console.log('Data Save', this.clientFormGroup.value);
    this.clientService.saveClient(this.clientFormGroup.value)
    .subscribe(res=>{
      console.log('Save Ok', res);
    }, error =>{
      console.log('error al guardar', error);
    });
  }

}
