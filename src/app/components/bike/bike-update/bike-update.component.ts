import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BikeService } from '../bike.service';
import { Bike } from '../bike.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bike-update',
  templateUrl: './bike-update.component.html',
  styleUrls: ['./bike-update.component.styl']
})
export class BikeUpdateComponent implements OnInit {
  bike: Bike;
  bikeFormUpdateGroup: FormGroup;
  constructor(private activatedRoute: ActivatedRoute,
              private bikeService: BikeService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.bikeFormUpdateGroup= this.formBuilder.group({
      id: [],
      model: ['', [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(5)
        ]],
      price: ['', Validators.required],
      serial: ['']
    });
   }
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID GET', id);
    this.bikeService.getBikeById(id)
    .subscribe(res => {
      console.warn('DATA UPDATE', res);
      this.bike = res;
      this.loadForm(this.bike);
    });
  }

  updateBike(){
    this.bikeService.updateBike(this.bikeFormUpdateGroup.value)
    .subscribe(res => {
      console.warn('Actualizando...');
      this.router.navigate(['/bike/bike-list']); //Redireccionar
    },err => console.warn(err));
  }

  private loadForm(bike: Bike){
    this.bikeFormUpdateGroup.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial
    });
  }

}
