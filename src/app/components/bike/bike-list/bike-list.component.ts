import { Component, OnInit } from '@angular/core';
import { BikeService } from '../bike.service';
import { Bike } from '../bike.interface';

@Component({
  selector: 'app-bike-list',
  templateUrl: './bike-list.component.html',
  styleUrls: ['./bike-list.component.styl']
})
export class BikeListComponent implements OnInit {
  bikeList: Bike[];
  constructor(private bikeService: BikeService) { }

  ngOnInit() {
    this.bikeService.query()
    .subscribe(res => {
      this.bikeList = res;
    });
    }
  }



