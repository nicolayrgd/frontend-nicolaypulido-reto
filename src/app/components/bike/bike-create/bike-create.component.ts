import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikeService } from '../bike.service';

@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.styl']
})
export class BikeCreateComponent implements OnInit {
bikeFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private bikeService: BikeService) {
    this.bikeFormGroup= this.formBuilder.group({
      model: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(5)
        ]],
      price: ['', Validators.required],
      serial: ['']
    });
  }

  ngOnInit() {

  }
  public saveBike(){
    console.log('Data save', this.bikeFormGroup.value);
    this.bikeService.saveBike(this.bikeFormGroup.value)
    .subscribe(res =>{
      console.log('Save Ok', res);
    }, error => {
      console.log('error al guardar', error);

    });
  }

}
