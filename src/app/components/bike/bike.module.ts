import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeUpdateComponent } from './bike-update/bike-update.component';
import { BikeCreateComponent } from './bike-create/bike-create.component';
import { BikeRoutingModule } from './bike-routing.module';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    BikeListComponent,
    BikeUpdateComponent,
    BikeCreateComponent
  ],
  imports: [
    CommonModule,
    BikeRoutingModule,
    ReactiveFormsModule

  ]
})
export class BikeModule { }
