import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bike } from './bike.interface';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  constructor(private http: HttpClient) { }

  public query(): Observable<Bike[]>{
    return this.http.get<Bike[]>(`${environment.END_POINT}/api/bike`)
    .pipe(map(res =>{
      return res;
    }));
  }
  public saveBike(bike: Bike): Observable<Bike> {
    return this.http.post<Bike>(`${environment.END_POINT}/bike`, bike)
    .pipe(map(res =>{
      return res;
    }));
  }
  public getBikeById(id: string): Observable<Bike>{
    return this.http.get<Bike>(`${environment.END_POINT}/api/bike/${id}`)
    .pipe(map(res =>{
      return res;
    }));   
  }
  public updateBike(bike: Bike): Observable<Bike>{
    return this.http.put<Bike>(`${environment.END_POINT}/bike`, bike)
    .pipe(map(res =>{
      return res;
    }));
  }
}