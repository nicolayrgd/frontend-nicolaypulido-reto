export interface Bike {
    id?: number;
    model: string;
    price: string;
    serial: string;
    description: string;
}
