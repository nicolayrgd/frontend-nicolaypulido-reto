import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Connect the main module
const routes: Routes = [

  {
    path: 'bike',
      loadChildren: () => import('./components/bike/bike.module')
      .then(modulo => modulo.BikeModule)
  },

  {
    path: '',
    redirectTo: 'bike',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
